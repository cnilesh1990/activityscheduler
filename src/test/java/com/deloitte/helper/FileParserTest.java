package com.deloitte.helper;

import com.deloitte.exception.SchedulerTechnicalException;
import com.deloitte.constants.Constants;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileParserTest {
    Map<String, Object> configuration = new HashMap<>();

    @BeforeAll
    public void setUp(){
        configuration.put(Constants.FILENAME_FIELD, "");
    }

    @Test(expected= SchedulerTechnicalException.class)
    public void testIfFilenameNotPresent(){
        new FileParser().parse(configuration);
    }

    @Test(expected = SchedulerTechnicalException.class)
    public void testFileFilenameIncorrect(){
        configuration.put(Constants.FILENAME_FIELD, "i.txt");
        new FileParser().parse(configuration);
    }

    @Test
    public void checkIfFilePopulatesList(){
        configuration.put(Constants.FILENAME_FIELD, "input.txt");
        assertThat(((List<String>)new FileParser().parse(configuration)).size(),is(20));
    }

    @AfterAll
    public void cleanUp(){
        this.configuration = null;
    }
}
