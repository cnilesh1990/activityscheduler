package com.deloitte.helper;

import com.deloitte.constants.Constants;
import com.deloitte.model.Activity;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.instanceOf;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import java.util.*;

public class ActivityParserTest {

    Map<String, Object> configuration = new HashMap<>();

    @BeforeAll
    public void setup(){
        List<String> fileList = new ArrayList<>();
        fileList.add("Duck Herding 60min");
        this.configuration.put(Constants.FILECONTENTS_FIELD, fileList);
    }

    @Test
    public void testHappyScenario(){
        assertThat(new ActivityParser().parse(this.configuration), instanceOf(LinkedHashSet.class));
    }

    @Test
    public void testFailure(){
        this.configuration.remove(Constants.FILECONTENTS_FIELD);
        assertThat(((Set<Activity>)new ActivityParser().parse(this.configuration)).size(), is(0));
    }

    @AfterAll
    public void cleanup(){
        this.configuration = null;
    }
}
