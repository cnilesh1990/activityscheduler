package com.deloitte.model;

public enum Sprint {
    SPRINT(15);
    private int value;

    private Sprint(int value){
        this.value = value;
    }

    public int getValue(){
        return this.value;
    }
}
