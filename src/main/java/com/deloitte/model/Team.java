package com.deloitte.model;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class Team {
    private String teamName;
    private Set<Activity> activities = new LinkedHashSet<>();

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Set<Activity> getActivities() {
        return activities;
    }

    public void setActivities(Set<Activity> activities) {
        this.activities = activities;
    }

    public int getTotalTime(){
        int totalTime = 0;
        for(Activity activity : this.activities){
            totalTime += activity.getDuration();
        }
        return totalTime;
    }
}
