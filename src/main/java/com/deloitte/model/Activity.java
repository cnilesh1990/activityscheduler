package com.deloitte.model;

import java.util.Objects;

public class Activity {
    private String title;
    private int duration;
    public static final String SPRINT = "sprint";
    public static final int SPRINT_DURATION = 15;

    public Activity(String title, int duration) {
        this.title = title;
        if(title.toLowerCase().endsWith(SPRINT)){
            this.duration = Sprint.SPRINT.getValue();
        } else {
            this.duration = duration;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Activity activity = (Activity) o;
        return Objects.equals(title, activity.title) &&
                Objects.equals(duration, activity.duration);
    }

    @Override
    public int hashCode() {

        return Objects.hash(title, duration);
    }

    public String getTitle() {
        return title;
    }

    public int getDuration() {
        return duration;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "title='" + title + '\'' +
                ", duration='" + duration + '\'' +
                '}';
    }
}
