package com.deloitte.constants;

public interface Constants {
    String FILENAME_FIELD = "filename";
    String FILECONTENTS_FIELD = "fileContents";
    String TEAMS = "teams";
}
