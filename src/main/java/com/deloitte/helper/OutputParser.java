package com.deloitte.helper;

import com.deloitte.constants.Constants;
import com.deloitte.model.Team;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

public class OutputParser implements IParser {

    public <T extends Object> T parse(Map<String, Object> configuration){
        if(configuration.get(Constants.TEAMS) != null){
            //Format is hours and minutes specifying time of day. eg : 09:00 am
            DateTimeFormatter timeFormatter = DateTimeFormatter
                    .ofPattern("hh:mm a");
            List<Team> teams = (List<Team>)configuration.get(Constants.TEAMS);
            LocalTime[] localtimeArr = new LocalTime[1];
            teams.forEach(team -> {
            LocalTime localTime = LocalTime.of(9,00);
            //Creating a localtime array since cannot modify objects in lambda.
                //it is effectively final or needs to be final.
                //Arrays can be modified since we are not changing the reference.
            localtimeArr[0] = localTime;
            System.out.println(team.getTeamName());
            team.getActivities().forEach(activity -> {
                System.out.println(localtimeArr[0].format(timeFormatter) + ":" + activity.getTitle());
                localtimeArr[0] = localtimeArr[0].plusMinutes(activity.getDuration());
            });
        });
        }
        return (T)null;
    }
}
