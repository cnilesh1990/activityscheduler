package com.deloitte.helper;


import com.deloitte.constants.Constants;
import com.deloitte.model.Activity;

import java.util.*;
import java.util.stream.Collectors;

public class ActivityParser implements IParser {
    public <T extends Object> T parse(Map<String, Object> configuration){
        Set<Activity> activities = new LinkedHashSet<>();
        if(configuration.get(Constants.FILECONTENTS_FIELD) != null){
            List<String> fileContents = (List<String>) configuration.get(Constants.FILECONTENTS_FIELD);
            activities = fileContents.stream().map(content -> {
                String[] contents = content.split(" ");
                Activity activity;
                if(contents[contents.length-1].contains("min"))
                    activity = new Activity(content, Integer.parseInt(contents[contents.length-1].replaceAll("[A-Za-z]","")));
                else
                    activity = new Activity(content,Activity.SPRINT_DURATION);
                return activity;
            }).collect(Collectors.toSet());
        }
        return (T)activities;
    }

    /**
     *
     * @param strArr
     * @param from
     * @param to
     * @return a single String object for an array of string.
     * This is required because there is no better utility method to do so at the moment
     */
    private static String getString(String[] strArr, int from , int to){
        String result = "";
        for(int i=from; i <= to ; i++ ){
            result += strArr[i]+" ";
        }
        return result;
    }
}
