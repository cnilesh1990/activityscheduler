package com.deloitte.helper;

import com.deloitte.exception.SchedulerTechnicalException;
import com.deloitte.constants.Constants;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Responsible for reading the lines from the files.
 */
public class FileParser implements IParser{

    /**
     *
     * @param configuration
     * @return: An object which in this case contains List<String>
     */
    public <T extends Object> T parse(Map<String,Object> configuration){
        String filename = (String)configuration.get(Constants.FILENAME_FIELD);
        if(filename == null){
            throw new SchedulerTechnicalException("File name not present");
        }
        List<String> fileContents = new ArrayList<>();
        File file = null;
        try {
            file = new File(filename); // Create file object which can later be used to get the URI
        }catch (Exception e){
            throw new SchedulerTechnicalException("Unable to read file. Verify the filename is correct or that the file is present", e);
        }

        try(Stream<String> stream = Files.lines(Paths.get(file.toURI()))){
            fileContents = stream
                    .collect(Collectors.toList());
        }catch(IOException e){
            throw new SchedulerTechnicalException("Unable to read file. Failure caused by error::" ,e);
        }
        return (T)fileContents;
    }
}
