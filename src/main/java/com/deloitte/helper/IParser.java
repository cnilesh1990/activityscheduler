package com.deloitte.helper;

import java.util.List;
import java.util.Map;

/**
 * General Interface to parse file, database rows, or any other type of object
 */
public interface IParser {

    //Accepts a list of configurations. In caseof files, accept filepath
    //Database can accept username,password,url,database_name etc.
    public <T extends Object> T parse(Map<String,Object> configurations);
}
