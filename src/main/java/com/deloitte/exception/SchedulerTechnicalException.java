package com.deloitte.exception;

/**
 * Creating an exception class since we need not want user to recover from technical exception
 */
public class SchedulerTechnicalException extends RuntimeException {

    public SchedulerTechnicalException(String msg){
        super(msg);
    }

    public SchedulerTechnicalException(String msg, Throwable e){
        super(msg, e);
    }
}
