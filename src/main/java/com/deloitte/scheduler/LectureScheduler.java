package com.deloitte.scheduler;

import com.deloitte.constants.Constants;
import com.deloitte.helper.ActivityParser;
import com.deloitte.helper.FileParser;
import com.deloitte.helper.IParser;
import com.deloitte.helper.OutputParser;
import com.deloitte.model.Activity;
import com.deloitte.model.Team;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class LectureScheduler {

    public void scheduleLectures(int teamCount, String fileLocation){
        Map<String, Object> configuration = new HashMap<String, Object>();
        List<Team> teams = new ArrayList<>();
        configuration.put(Constants.FILENAME_FIELD, fileLocation);
        List<String> fileContents = (List<String>)new FileParser().parse(configuration); // Read contents of the file to list
        configuration.put(Constants.FILECONTENTS_FIELD, fileContents);
        Set<Activity> activities = (Set<Activity>)new ActivityParser().parse(configuration); // copy the meaningful data from the lines to create Activity Object
        IntStream.range(0,teamCount).forEach(n->{                  // This code creates team object based on the count specified
            Team team = new Team();
            team.setTeamName("Team "+(n+1));
            teams.add(team);
        });
        scheduleLecture(activities, teams, 180); // Schedule activities for morning session. Morning session should be maximum 3 hours(180 minutes)
        IntStream.range(0, teamCount).forEach(n -> {
            Activity activity = new Activity("Lunch Break", 60);
            teams.get(n).getActivities().add(activity);
        });
        scheduleLecture(activities, teams, 480); // Schedule activities for evening session. Morning session should be maximum 3 hours(180 minutes)
                                                        // This is 480 minutes because team accumulates the total time in the team object. So 3(Morning)+1(Lunch)+4(Afternoon)=480 mins
        IntStream.range(0, teamCount).forEach(n -> {
            Activity activity = new Activity("Presentation", 60);
            teams.get(n).getActivities().add(activity);
        });
        configuration.put(Constants.TEAMS, teams);
        IParser outputParser = new OutputParser(); //responsible for printing on the console. Can be swapper to pass the output to file.
        outputParser.parse(configuration);
    }

    /**
     *
     * @param activities
     * @param teams
     * @param time
     * Schedules the activities for each team in iteration.
     */
    private void scheduleLecture(Set<Activity> activities, List<Team> teams, int time){
        for(Team team : teams){
            Iterator<Activity> activityIter = activities.iterator();
            while(activityIter.hasNext()){
                Activity activity = activityIter.next();
                //Can add activity since the time of the activity is less than the time remaining for the schedule to complete
                if(team.getTotalTime() < time){
                    if(team.getTotalTime() + activity.getDuration() < time){
                        team.getActivities().add(activity);
                        activityIter.remove();
                    }else if(team.getTotalTime() + activity.getDuration() == time){ // add activity and break since cannot take more activity
                        team.getActivities().add(activity);
                        activityIter.remove();
                        break;
                    }else {
                        continue;
                    }
                }
            }
        }
    }

}
