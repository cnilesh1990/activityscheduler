import com.deloitte.scheduler.LectureScheduler;

public class Main {

    public static void main(String[] args) {
        new LectureScheduler().scheduleLectures(2, "input.txt");
    }
}
