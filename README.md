# Scheduler

This code is expected to schedule the events specified in the txt file. The code starts with getting the number of teams participating and the name of the input file where the events will be specified. The code will be triggered from `Main.java` file


# Project Structure

There are model classes defined for storing `Activity ` as well as the `Team` participating in the activity. A custom exception class(`SchedulerTechnicalException`) has been defined which extends `RuntimeException`, since the exceptions are fatal and the program cannot proceed unless the exceptions are corrected. `Activity` model class overrides the `equals` and `hashCode` method in order to ensure that the activities are not repeated across team. The `Activity` is then stored in `Set` interface to ensure the same. Lecture Scheduler class contains the logic for ensuring that all the conditions have been met and the schedule follows the same.

`Test` has been written for `File` parsing as well as `Activity` Parsing. Appropriate Junit jars have been downloaded using maven.

## Assumptions

1. Lunch can be scheduled anytime before 12pm, but proper care has been taken to start after 11:30pm only.
2. It is possible that some of the activities may not be divided amongst teams.
3. Different order of schedules in different run is possible. But care has been taken that all of the conditions have been met.

